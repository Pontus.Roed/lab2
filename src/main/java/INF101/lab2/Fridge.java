package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    private int maxItems = 20;
    private List<FridgeItem> items;

    public Fridge() {
        items = new ArrayList<FridgeItem>();
    }

    @Override
    public int totalSize() {
        return maxItems;
    }


    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() >= maxItems){
            return false;
        }
        return items.add(item);
    }


    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)){
            throw new NoSuchElementException("This item is not in the fridge");
        }
        items.remove(item);
    }


    @Override
    public void emptyFridge() {
        items.clear();
        
    }


    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        for (FridgeItem item : expiredItems) {
            items.remove(item);
        }
        return expiredItems;
    }
}
